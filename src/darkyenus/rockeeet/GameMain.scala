package darkyenus.rockeeet

import darkyenus.riverpebbleframework.Game
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import darkyenus.riverpebbleframework.resource.Resource
import javax.swing.{JOptionPane, JFileChooser}
import com.badlogic.gdx.tools.imagepacker.TexturePacker2
import com.badlogic.gdx.{Input, Gdx}
import org.lwjgl.opengl.Display
import org.lwjgl.input.{Mouse, Cursor}
import java.nio.IntBuffer
import org.lwjgl.BufferUtils

/**
 * Private property.
 * User: Darkyen
 * Date: 13/01/14
 * Time: 20:49
 */
object GameMain extends App {

  var vSync = true
  var fullScreen = false

  lazy val emptyCursor = {
    val min = Cursor.getMinCursorSize
    val tmp = BufferUtils.createIntBuffer(min*min)
    new Cursor(min,min,min/2,min/2,1,tmp,null)
  }
  def hideCursor(){
    Mouse.setNativeCursor(emptyCursor)
  }

  def showCursor(){
    Mouse.setNativeCursor(null)
  }

  val displayModes = LwjglApplicationConfiguration.getDisplayModes
  displayModes.foreach(println(_))
  var index = -1
  var activeDisplayMode = LwjglApplicationConfiguration.getDesktopDisplayMode

  Game(new LwjglApplicationConfiguration{


    width = activeDisplayMode.width
    height = activeDisplayMode.height

    foregroundFPS = activeDisplayMode.refreshRate max 60
    backgroundFPS = 5
    title = "Rockeeet"
    fullscreen = fullScreen
    vSyncEnabled = vSync
    samples = 0
  },
    cycle => {
          darkyenus.rockeeet.registerResources()
          Resource.finishLoading()
          hideCursor()
          cycle(RockeeetState)
      ()=>{
        Resource.dispose()
      }
    }

  )
}
