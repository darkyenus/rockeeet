package darkyenus.rockeeet

import darkyenus.riverpebbleframework.{Render2D, Render2DContext, GameState}
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.{Graphics, Input, Gdx}

/**
 * Private property.
 * User: Darkyen
 * Date: 17/01/14
 * Time: 08:04
 */
object PauseState extends GameState {

  val FontBaseScale = 0.00027f

  var nextDisplayMode:Option[Graphics.DisplayMode] = None

  val resolutionLabel = new ClickableLabel(0.5f,0.1f,1f,()=>nextDisplayMode.getOrElse(GameMain.activeDisplayMode).width+" x "+nextDisplayMode.getOrElse(GameMain.activeDisplayMode).height,(label)=>{
    GameMain.index += 1
    GameMain.index %= GameMain.displayModes.length
    nextDisplayMode = Some(GameMain.displayModes(GameMain.index))
  })
  val windowedLabel = new ClickableLabel(0.5f-0.2f,0.2f,1f,()=>if(Gdx.graphics.isFullscreen)"Fullscreen" else "Windowed",(label)=>{GameMain.fullScreen = !GameMain.fullScreen;Gdx.graphics.setDisplayMode(GameMain.activeDisplayMode.width,GameMain.activeDisplayMode.height,GameMain.fullScreen)})
  val vSyncLabel = new ClickableLabel(0.5f+0.2f,0.2f,1f,()=>if(GameMain.vSync)"VSync" else "No VSync",(label)=>{GameMain.vSync = !GameMain.vSync;Gdx.graphics.setVSync(GameMain.vSync)})
  val applyResolutionLabel = new ClickableLabel(0.5f,0.3f,1f,()=>if(nextDisplayMode.isDefined)"Apply" else "",(label)=>{
    nextDisplayMode match {
      case Some(displayMode) =>
        Gdx.graphics.setDisplayMode(displayMode.width,displayMode.height,GameMain.fullScreen)
        nextDisplayMode = None
      case None =>
    }
  },Color.GREEN,new Color(0f,0.9f,0.9f,1f))

  var exit = false
  var unpause = false
  val unpauseLabel = new ClickableLabel(0.5f-0.2f,0.4f,1.1f,()=>"Unpause",(label)=>{unpause = true})
  val quitLabel = new ClickableLabel(0.5f+0.2f,0.4f,1.1f,()=>"Quit",(label)=>{exit = true},hoverColor = Color.RED)

  private val elements:Array[ClickableLabel] = Array(resolutionLabel,windowedLabel,vSyncLabel,applyResolutionLabel,unpauseLabel,quitLabel)


  override def touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean = {
    val x = screenX.toFloat/Gdx.graphics.getWidth
    val y = screenY.toFloat/Gdx.graphics.getWidth
    elements.find(_.contains(x,y)) match {
      case Some(element) =>
        element.action(element)
        true
      case None =>
        false
    }
  }

  override def keyUp(keycode: Int): Boolean = {
    keycode match {
      case Input.Keys.ESCAPE =>
        unpause = true
        true
      case Input.Keys.Q =>
        exit = true
        true
      case _ =>
        false
    }
  }

  def process(delta: Float): Option[GameState] = {
    if(exit){
      None
    }else if(unpause){
      Render2D.clearScreen()
      unpause = false
      nextDisplayMode = None
      GameMain.hideCursor()
      Some(RockeeetState)
    }else{
      Render2D.clearScreen()
      render(Gdx.graphics.getWidth)(context =>{
        for(element <- elements){
          element.render(context)
        }
      })
      Some(PauseState)
    }
  }

  class ClickableLabel(x:Float,y:Float,scale:Float,var text:()=>String,var action:(ClickableLabel)=>Unit,var color:Color = Color.WHITE, var hoverColor:Color = Color.LIGHT_GRAY) {

    var width = 0f
    var height = 0f

    def render(render:Render2DContext){
      val text = this.text().toLowerCase
      MainFont.setScale(FontBaseScale*scale)
      val bounds = MainFont.getBounds(text)
      if(contains(Gdx.input.getX.toFloat/Gdx.graphics.getWidth,Gdx.input.getY.toFloat/Gdx.graphics.getWidth)){
        MainFont.setColor(hoverColor)
      }else{
        MainFont.setColor(color)
      }
      val lastBounds = MainFont.draw(render,text,x-bounds.width/2,y-bounds.height/2)
      width = lastBounds.width
      height = lastBounds.height
    }

    def contains(x:Float,y:Float):Boolean = {
      this.x - width/2 <= x && this.x + width/2 >= x && this.y - height/2 <= y && this.y + height/2 >= y
    }
  }

  override def resize(width: Int, height: Int){RockeeetState.resize(width,height)}
}
