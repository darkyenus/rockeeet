package darkyenus.rockeeet

import darkyenus.riverpebbleframework.{Render2DContext, Render2D, GameState}
import com.badlogic.gdx.{Input, Gdx}
import com.badlogic.gdx.math.{Rectangle, Circle}
import scala.annotation.tailrec
import com.badlogic.gdx.graphics.g2d.ParticleEffect
import com.badlogic.gdx.graphics.Color
import scala.Some
import darkyenus.riverpebbleframework.resource.TextureRegionResource
import darkyenus.riverpebbleframework.resource.TextureRegionResource.TextureRegionResource
import java.security.SecureRandom

/**
 * Private property.
 * User: Darkyen
 * Date: 13/01/14
 * Time: 20:55
 */
object RockeeetState extends GameState {

  val Random = new SecureRandom()
  
  private var activeRocket:Option[SimpleRocket] = None
  var gameOverTimeout = 1f
  var pauseRequested = false
  private var asteroids = Set[Asteroid]()
  private val minAsteroids = 5
  private val maxAsteroids = 40
  val smallestAsteroid = 0.03f
  private val StartingAsteroidBaseSpeed = 0.2f
  private var asteroidBaseSpeed = StartingAsteroidBaseSpeed
  var effects = Set[ParticleEffect]()
  var powerups = Set[Powerup]()
  var bullets = Set[LaserShot]()

  var score = 0
  var multiplier = 1
  val ScoreFontBaseScale = 0.00027f

  private var screenRect = new Rectangle(-0.4f,-0.4f,1.8f,(Gdx.graphics.getHeight.toFloat/Gdx.graphics.getWidth)+0.8f)

  override def keyTyped(character: Char): Boolean = {
    character match {
      case 'c' =>
        powerups = powerups + new CoinNotQuitePowerup(Random.nextFloat()*0.9f+0.05f,-0.15f)
      case 's' =>
        powerups = powerups + new ShieldPowerup(Random.nextFloat()*0.9f+0.05f,-0.15f)
      case 'b' =>
        powerups = powerups + new SpeedPowerup(Random.nextFloat()*0.9f+0.05f,-0.15f)
      case 'g' =>
        powerups = powerups + new StarPowerup(Random.nextFloat()*0.9f+0.05f,-0.15f)
      case 'h' =>
        powerups = powerups + new HealPowerup(Random.nextFloat()*0.9f+0.05f,-0.15f)
      case 'l' =>
        powerups = powerups + new BulletPowerup(Random.nextFloat()*0.9f+0.05f,-0.15f)
      case 'm' =>
        asteroids = asteroids + new Asteroid(Random.nextFloat()*0.9f+0.05f,-0.2f,Random.nextFloat()*Random.nextFloat()*0.2f+0.04f,(Random.nextFloat()-0.5f)*0.03f,asteroidBaseSpeed+Random.nextFloat()*0.06f,(Random.nextFloat()-0.5f)*200.5f)
      case 'f' =>
        println(Gdx.graphics.getFramesPerSecond)
      case 'p' =>
        multiplier *= 2
      case _ =>
    }
    true
  }

  override def keyUp(keycode: Int): Boolean = {
    keycode match {
      case Input.Keys.ESCAPE =>
        pauseRequested = true
      case _ =>
    }
    true
  }

  def process(delta: Float): Option[GameState] = {
    Render2D.clearScreen()
    if(activeRocket.isEmpty){
      gameOverTimeout -= Gdx.graphics.getDeltaTime
      if(gameOverTimeout < 0){
        activeRocket = Some(new SimpleRocket(Gdx.input.getX.toFloat/Gdx.graphics.getWidth,Gdx.input.getY.toFloat/Gdx.graphics.getWidth))
        activeRocket.get.immunity = 3f
        activeRocket.get.shield = 3f
        score = 0
        multiplier = 1
        asteroidBaseSpeed = StartingAsteroidBaseSpeed
      }
    }
    render(Gdx.graphics.getWidth)(
      render =>{
        update(render)
      }
    )
    if(pauseRequested){
      pauseRequested = false
      GameMain.showCursor()
      Some(PauseState)
    }else{
      Some(RockeeetState)
    }
  }

  var spawnLoopTimeout = 0f
  var lootCraze = 0f
  def update(render:Render2DContext){
    activeRocket.foreach(_.updateMovement())

    spawnLoopTimeout -= Gdx.graphics.getDeltaTime
    lootCraze -= Gdx.graphics.getDeltaTime
    while(spawnLoopTimeout < 0){
      if((asteroids.size < maxAsteroids && Random.nextFloat() > 0.91f)||asteroids.size < minAsteroids){
        asteroids = asteroids + new Asteroid(Random.nextFloat()*0.9f+0.05f,-0.2f,Random.nextFloat()*Random.nextFloat()*0.2f+0.04f,(Random.nextFloat()-0.5f)*0.03f,asteroidBaseSpeed+Random.nextFloat()*0.06f,(Random.nextFloat()-0.5f)*200.5f)
      }
      if(Random.nextFloat() > 0.999f || (lootCraze > 0 && Random.nextFloat() > 0.8f)){
        powerups = powerups + new ShieldPowerup(Random.nextFloat()*0.9f+0.05f,-0.15f)
      }
      if(Random.nextFloat() > 0.995f || (lootCraze > 0 && Random.nextFloat() > 0.8f)){
        powerups = powerups + new SpeedPowerup(Random.nextFloat()*0.9f+0.05f,-0.15f)
      }
      if((Random.nextFloat() > 0.995f || (lootCraze > 0 && Random.nextFloat() > 0.8f)) && !activeRocket.forall(rocket => rocket.lives == rocket.maxLives)){
        powerups = powerups + new HealPowerup(Random.nextFloat()*0.9f+0.05f,-0.15f)
      }
      if(Random.nextFloat() > 0.995f || (lootCraze > 0 && Random.nextFloat() > 0.8f)){
        powerups = powerups + new BulletPowerup(Random.nextFloat()*0.9f+0.05f,-0.15f)
      }
      if(Random.nextFloat() > 0.9995f && lootCraze <= 0){
        powerups = powerups + new StarPowerup(Random.nextFloat()*0.9f+0.05f,-0.15f)
      }
      if(Random.nextFloat() > 0.98f && lootCraze <= 0){
        powerups = powerups + new CoinNotQuitePowerup(Random.nextFloat()*0.9f+0.05f,-0.15f)
      }
      spawnLoopTimeout += 1f/20f
    }
    asteroids.foreach(_.update())
    powerups.foreach(_.update())
    bullets.foreach(_.update())
    asteroids = asteroids.filter(asteroid => screenRect.contains(asteroid.shape.x,asteroid.shape.y))
    powerups = powerups.filterNot(_.remove)
    bullets = bullets.filter(_.keep)

    processCollisions()
    var newResidueAsteroids = Set[Asteroid]()
    asteroids = asteroids.filter(_.explode match {
      case None =>
        true
      case Some(residue) =>
        newResidueAsteroids = newResidueAsteroids ++ residue
        false
    })
    asteroids = asteroids ++ newResidueAsteroids

    asteroids.foreach(_.render(render))
    powerups.filterNot(_.isInstanceOf[CoinNotQuitePowerup]).foreach(_.render(render))
    powerups.filter(_.isInstanceOf[CoinNotQuitePowerup]).foreach(_.render(render))
    bullets.foreach(_.render(render))
    activeRocket.foreach(_.render(render))
    effects.foreach(_.draw(render,Gdx.graphics.getDeltaTime))
    effects = effects.filterNot(_.isComplete)

    MainFont.setColor(Color.WHITE)
    MainFont.setScale(ScoreFontBaseScale)
    val drawnTo = MainFont.draw(render,"%08d".format(score),0.01f,0.01f).width
    val bounceSin = Math.sin(((System.currentTimeMillis()%800)/ 800.0)*Math.PI*2).toFloat
    val modifierSteepness = -0.01f
    val bounceRange = ScoreFontBaseScale*0.9f
    val multiplierBounceModifier = (bounceRange*bounceRange*(1-multiplier))/(modifierSteepness+bounceRange*(1-multiplier))
    val redScale = ScoreFontBaseScale+bounceSin*multiplierBounceModifier
    MainFont.setColor(Color.RED)
    val unscaledLineHeight = MainFont.getLineHeight
    MainFont.setScale(redScale)
    MainFont.draw(render,"x%02d".format(multiplier),0.01f+drawnTo,0.01f+unscaledLineHeight/2f-MainFont.getLineHeight/2f)
  }

  def processCollisions(){
    @tailrec
    def processAsteroid(head:Asteroid,tail:Set[Asteroid]){
      if(!tail.isEmpty){
        for(tailAsteroid <- tail){
          if(head.shape.overlaps(tailAsteroid.shape)){
            tailAsteroid.onCollision(head)
            head.onCollision(tailAsteroid)
          }
        }
        processAsteroid(tail.head,tail.tail)
      }
    }
    if(asteroids.size > 1){
      processAsteroid(asteroids.head,asteroids.tail)
    }

    if(activeRocket.isDefined){
      val rocket = activeRocket.get
      for(asteroid <- asteroids){
        if(rocket.shape.overlaps(asteroid.shape)){
          asteroid.onCollision(rocket)
          rocket.onCollision(asteroid)
        }
      }
      for(powerup <- powerups){
        if(rocket.shape.overlaps(powerup.shape)){
          powerup.onCollision(rocket)
          rocket.onCollision(powerup)
        }
      }
    }

    for(bullet <- bullets){
      for(asteroid <- asteroids){
        if(bullet.shape.overlaps(asteroid.shape)){
          bullet.onCollision(asteroid)
          asteroid.onCollision(bullet)
        }
      }
    }
  }

  override def resize(width: Int, height: Int){
    screenRect = new Rectangle(-0.2f,-0.2f,1.4f,(Gdx.graphics.getHeight.toFloat/Gdx.graphics.getWidth)*1.4f)
  }

  class SimpleRocket(x:Float,y:Float, val width:Float = 0.1f, val height:Float = 0.1f) extends Collidable {

    var rocketTexture = RocketTexture
    val shape:Circle = new Circle(x,y,(width+height)/4f*0.08f)
    val maxLives = 4
    var lives = maxLives
    var shield = 0f
    var immunity = 0f
    var damageBlink = 0f
    var extraSpeed = 0f
    var shooting = 0f

    var shootCooldown = 0f
    var left = true

    var speed = 1f
    val thrustEffect = ThrustEffect.copy

    def render(context:Render2DContext){
      if(shield > 0){
        val shieldWidth = width*1.7f
        val shieldHeight = height*1.7f
        if(shield < 2){
          context.setColor(0.1f,0.7f,1f,shield/2f)
        }else{
          context.setColor(0.1f,0.7f,1f,1f)
        }
        context.draw(RocketTextureShield.get,shape.x-shieldWidth/2,shape.y-shieldHeight/2,shieldWidth,shieldHeight)
      }
      context.setColor(Color.WHITE)
      val thrustWidth = width*0.3f
      val thrustHeight = 0.1f*speed
      context.draw(ThrustTextureBase.get,shape.x-thrustWidth/2,shape.y+thrustHeight+height/2,thrustWidth,-thrustHeight)
      context.draw(ThrustTextureDetail.get,shape.x-thrustWidth/2,shape.y+thrustHeight+height/2,thrustWidth,-thrustHeight*0.5f)
      thrustEffect.setPosition(shape.x,shape.y)
      thrustEffect.draw(context,Gdx.graphics.getDeltaTime)
      if(damageBlink > 0 && (System.currentTimeMillis()/150)%2 == 0){
        context.setColor(Color.RED)
      }else{
        context.setColor(Color.WHITE)
      }
      context.draw(rocketTexture.get,shape.x-width/2,shape.y-height/2,width,height)
      if(lives < maxLives-1){
        if(rocketTexture == RocketTexture || rocketTexture == RocketTextureDamaged){
          context.draw(Scratch1Texture.get,shape.x-0.035f,shape.y+0.02f,0.02f,-0.02f)
        }else if(rocketTexture == RocketTextureLeft){
          context.draw(Scratch1Texture.get,shape.x-0.035f-0.001f,shape.y+0.02f,0.02f-0.001f,-0.02f)
        }else if(rocketTexture == RocketTextureRight){
          context.draw(Scratch1Texture.get,shape.x-0.035f+0.003f,shape.y+0.02f,0.02f-0.001f,-0.02f)
        }
      }
      if(lives < maxLives){
        if(rocketTexture == RocketTexture || rocketTexture == RocketTextureDamaged){
          context.draw(Scratch2Texture.get,shape.x+0.035f,shape.y+0.02f,-0.02f,-0.02f)
        }else if(rocketTexture == RocketTextureLeft){
          context.draw(Scratch2Texture.get,shape.x+0.035f-0.003f,shape.y+0.02f,-0.02f+0.001f,-0.02f)
        }else if(rocketTexture == RocketTextureRight){
          context.draw(Scratch2Texture.get,shape.x+0.035f+0.001f,shape.y+0.02f,-0.02f+0.001f,-0.02f)
        }
      }
    }

    def updateMovement(){
      shield -= Gdx.graphics.getDeltaTime
      immunity -= Gdx.graphics.getDeltaTime
      damageBlink -= Gdx.graphics.getDeltaTime
      extraSpeed -= Gdx.graphics.getDeltaTime
      shooting -= Gdx.graphics.getDeltaTime
      shootCooldown -= Gdx.graphics.getDeltaTime

      var k = if(lives >= 2) 0.2f else 0.5f
      if(extraSpeed > 0){
        k *= 0.2f
      }
      val interpolation = (-k / (Gdx.graphics.getDeltaTime + k)) + 1
      val newX = shape.x+(Gdx.input.getX/Gdx.graphics.getWidth.toFloat - shape.x)*interpolation
      val newY = shape.y+(Gdx.input.getY/Gdx.graphics.getWidth.toFloat - shape.y)*interpolation

      val deltaX = shape.x-newX
      val threshold = 0.00075f
      if(lives <= 1){
        rocketTexture = RocketTextureDamaged
      }else if(deltaX > threshold){
        rocketTexture = RocketTextureLeft
      }else if(deltaX < -threshold){
        rocketTexture = RocketTextureRight
      }else{
        rocketTexture = RocketTexture
      }

      val deltaY = shape.y-newY
      speed = Math.max((deltaY*100+5f)/6f,0.05f)

      shape.x = newX
      shape.y = newY

      if(shooting > 0){
        if(shootCooldown < 0){
          bullets = bullets + new LaserShot(shape.x + (if(left) -1 else 1) * 0.048f + 0.014f,shape.y-0.005f,-1.80f,0.04f,false)
          left = !left
          shootCooldown += 0.2f
        }
      }
    }

    def onCollision(in: Collidable){
      in match {
        case harmful:Harmful =>
          if(immunity <= 0){
            lives -= harmful.damage
            immunity = 1f
            damageBlink = 1f
            multiplier = 1
            if(lives <= 0){
              activeRocket = None
              val effect = ExplosionEffect.copy
              effect.setPosition(shape.x,shape.y)
              effect.start()
              effects = effects + effect
              gameOverTimeout = 5f
            }
          }
        case _ =>
      }

    }
  }

  class Asteroid(x:Float,y:Float,size:Float,xVel:Float,yVel:Float,aVel:Float) extends Collidable with Harmful{

    val shape:Circle = new Circle(x,y,size/2f)
    private var angle = Random.nextInt(360).toFloat
    private val texture = if(size > 0.05f)AsteroidBig else AsteroidSmall
    var immunity = 0f

    def render(context:Render2DContext){
      context.setColor(Color.WHITE)
      val dimensionMax = Math.max(texture.getRegionWidth,texture.getRegionHeight).toFloat
      val drawWidth = (texture.getRegionWidth/dimensionMax)*size
      val drawHeight = (texture.getRegionHeight/dimensionMax)*size
      context.draw(texture.get,shape.x-size/2f,shape.y-size/2f,size/2f,size/2f,drawWidth,drawHeight,1f,1f,angle)
    }

    def update(){
      shape.x += xVel * Gdx.graphics.getDeltaTime
      shape.y += yVel * Gdx.graphics.getDeltaTime
      angle += aVel * Gdx.graphics.getDeltaTime
      immunity -= Gdx.graphics.getDeltaTime
    }

    private var exploded = false

    def onCollision(in: Collidable){
      in match {
        case asteroid:Asteroid =>
          if(immunity < 0){
            exploded = true
          }
        case _:LaserShot =>
          exploded = true
        case _:SimpleRocket =>
          exploded = true
        case _ =>
      }
    }

    def explode:Option[Set[Asteroid]] = {
      if(exploded){
        var residue = Set[Asteroid]()
        var remainingSize = size
        while(remainingSize > smallestAsteroid){
          val residueSize = Math.min(Random.nextFloat()*size,remainingSize)
          remainingSize -= residueSize
          if(residueSize >= smallestAsteroid){
            val residueAsteroid = new Asteroid(shape.x,shape.y,residueSize,(Random.nextFloat()-0.5f)*0.06f,(Random.nextFloat()-0.1f)*0.06f+asteroidBaseSpeed/2f,aVel*2f*Random.nextFloat())
            residueAsteroid.immunity = 1f
            residue = residue + residueAsteroid
          }
        }
        val effect = CrashEffect.copy
        effect.setPosition(shape.x,shape.y)
        effect.getEmitters.get(0).getEmission.setHigh(size*500f)
        effect.start()
        effects = effects + effect
        Some(residue)
      }else{
        None
      }
    }

    def damage: Int = if(size > 0.08f) 2 else 1
  }

  class LaserShot(x:Float,y:Float,val yVel:Float,val size:Float = 0.04f,green:Boolean) extends Collidable {

    var exists = true

    val texture = if(green)GreenLaserTexture.get else RedLaserTexture.get

    def update(){
      shape.y += yVel*Gdx.graphics.getDeltaTime
    }

    def render(context:Render2DContext){
      context.setColor(Color.WHITE)
      val dimensionMax = Math.max(texture.getRegionWidth,texture.getRegionHeight).toFloat
      val drawWidth = (texture.getRegionWidth/dimensionMax)*size
      val drawHeight = (texture.getRegionHeight/dimensionMax)*size
      context.draw(texture,shape.x-size/2f,shape.y-size/2f,size/2f,size/2f,drawWidth,drawHeight,1f,1f,0f)
    }

    val shape: Circle = new Circle(x,y,size)

    def onCollision(in: Collidable){
      exists = false
    }

    def keep:Boolean = {
      if(exists){
        screenRect.contains(shape.x,shape.y)
      }else{
        false
      }
    }
  }

  abstract class Powerup(x:Float,y:Float,size:Float,val xVel:Float, val yVel:Float,val aVel:Float) extends Collidable {

    private var angle = Random.nextFloat()*360f
    private var exists = true

    def update(){
      shape.x += xVel*Gdx.graphics.getDeltaTime
      shape.y += yVel*Gdx.graphics.getDeltaTime
      angle += aVel*Gdx.graphics.getDeltaTime
    }

    def render(context:Render2DContext){
      val texture = image.get
      context.setColor(Color.WHITE)
      val dimensionMax = Math.max(texture.getRegionWidth,texture.getRegionHeight).toFloat
      val drawWidth = (texture.getRegionWidth/dimensionMax)*size
      val drawHeight = (texture.getRegionHeight/dimensionMax)*size
      context.draw(texture,shape.x-size/2f,shape.y-size/2f,size/2f,size/2f,drawWidth,drawHeight,1f,1f,angle)
    }

    val shape: Circle = new Circle(x,y,size*0.8f)

    def onCollision(in: Collidable){
      in match {
        case rocket:SimpleRocket =>
          exists = false
          applyPowerup(rocket)
        case _ =>
      }
    }

    def image:TextureRegionResource.TextureRegionResource

    def applyPowerup(rocket:SimpleRocket)

    def remove:Boolean = {
      if(exists){
        if(!screenRect.contains(shape.x,shape.y)){
          onDisappear()
          true
        }else{
          false
        }
      }else{
        true
      }
    }

    def onDisappear(){}
  }

  class ShieldPowerup(x:Float,y:Float) extends Powerup(x,y,0.04f,0f,asteroidBaseSpeed*0.7f,Random.nextFloat()*50f){

    def image: TextureRegionResource = DefensiveCollectibleTexture

    def applyPowerup(rocket: SimpleRocket){
      if(rocket.shield > 0){
        rocket.shield += 5f
      }else{
        rocket.shield = 5f
      }
      rocket.immunity = Math.max(rocket.shield,rocket.immunity)
    }
  }

  class BulletPowerup(x:Float,y:Float) extends Powerup(x,y,0.04f,0f,asteroidBaseSpeed*0.8f,Random.nextFloat()*50f){

    def image: TextureRegionResource = OffensiveCollectibleTexture

    def applyPowerup(rocket: SimpleRocket){
      if(rocket.shooting > 0){
        rocket.shooting += 5f
      }else{
        rocket.shooting = 5f
        rocket.shootCooldown = 0f
      }
    }
  }

  class SpeedPowerup(x:Float,y:Float) extends Powerup(x,y,0.04f,0f,asteroidBaseSpeed*0.9f,Random.nextFloat()*50f){

    def image: TextureRegionResource = DefensiveCollectibleTexture

    def applyPowerup(rocket: SimpleRocket){
      if(rocket.extraSpeed > 0){
        rocket.extraSpeed += 5f
      }else{
        rocket.extraSpeed = 5f
      }
      rocket.thrustEffect.start()
    }
  }

  class HealPowerup(x:Float,y:Float) extends Powerup(x,y,0.04f,0f,asteroidBaseSpeed*0.4f,Random.nextFloat()*50f){

    def image: TextureRegionResource = DefensiveCollectibleTexture

    def applyPowerup(rocket: SimpleRocket){
      rocket.lives = Math.min(rocket.maxLives,rocket.lives + 1)
      val effect = HealEffect.copy
      effect.setPosition(rocket.shape.x,rocket.shape.y)
      effect.start()
      effects = effects + effect
    }
  }

  class CoinNotQuitePowerup(x:Float,y:Float) extends Powerup(x,y,0.04f,0f,asteroidBaseSpeed*0.4f,Random.nextFloat()*50f){
    def image: TextureRegionResource = CoinCollectibleTexture

    def applyPowerup(rocket: SimpleRocket){
      val effect = CoinEffect.copy
      effect.setPosition(rocket.shape.x,rocket.shape.y)
//      if(Random.nextFloat() < 0.05f){
//        effect.getEmitters.get(0).getScale.setHigh(0.02f,0.18f)
//      }
      effect.start()
      effects = effects + effect

      score += multiplier
      multiplier += 1
      asteroidBaseSpeed += 0.003f
    }

    override def onDisappear() {
      multiplier = 1
    }
  }

  class StarPowerup(x:Float,y:Float) extends Powerup(x,y,0.04f,0f,asteroidBaseSpeed*0.4f,Random.nextFloat()*50f){
    def image: TextureRegionResource = StarCollectibleTexture

    def applyPowerup(rocket: SimpleRocket){
      multiplier *= 2
      val effect = StarEffect.copy
      effect.setPosition(rocket.shape.x,rocket.shape.y)
//      if(Random.nextFloat() < 0.05f){
//        effect.getEmitters.get(0).getScale.setHigh(0.02f,0.24f)
//      }
      effect.start()
      effects = effects + effect
    }
  }

  trait Collidable {

    def shape:Circle

    def onCollision(in:Collidable)
  }

  trait Harmful {
    def damage:Int
  }
}
