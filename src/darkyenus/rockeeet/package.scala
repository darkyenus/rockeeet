package darkyenus

import darkyenus.riverpebbleframework.resource.{BitmapFontResource, ParticleEffectResource, TextureRegionResource, TextureAtlasResource}
import com.badlogic.gdx.Gdx
import darkyenus.riverpebbleframework.Render2D

/**
 * Private property.
 * User: Darkyen
 * Date: 22/01/14
 * Time: 17:10
 */
package object rockeeet {
  val RockeeetAtlas = TextureAtlasResource from (Gdx.files.internal("Rockeeet.atlas"),conf => conf.flip = true)
  val RocketTexture = TextureRegionResource from (RockeeetAtlas, "player")
  val RocketTextureLeft = TextureRegionResource from (RockeeetAtlas, "playerLeft")
  val RocketTextureRight = TextureRegionResource from (RockeeetAtlas, "playerRight")
  val RocketTextureDamaged = TextureRegionResource from (RockeeetAtlas, "playerDamaged")
  val RocketTextureShield = TextureRegionResource from (RockeeetAtlas, "shield")
  val AsteroidSmall = TextureRegionResource from (RockeeetAtlas, "meteorSmall")
  val AsteroidBig = TextureRegionResource from (RockeeetAtlas, "meteorBig")
  val CrashEffect = ParticleEffectResource from (Gdx.files.internal("crash.effect"),RockeeetAtlas)
  val ExplosionEffect = ParticleEffectResource from (Gdx.files.internal("explosion.effect"), RockeeetAtlas)
  val ThrustEffect = ParticleEffectResource from (Gdx.files.internal("thrust.effect"), RockeeetAtlas)
  val HealEffect = ParticleEffectResource from (Gdx.files.internal("heal.effect"), RockeeetAtlas)
  val CoinEffect = ParticleEffectResource from (Gdx.files.internal("coin.effect"), RockeeetAtlas)
  val StarEffect = ParticleEffectResource from (Gdx.files.internal("star.effect"), RockeeetAtlas)
  val ThrustTextureBase = TextureRegionResource from (RockeeetAtlas, "jetFlame1")
  val ThrustTextureDetail = TextureRegionResource from (RockeeetAtlas, "jetFlame2")
  val DefensiveCollectibleTexture = TextureRegionResource from (RockeeetAtlas, "boxCoin")
  val OffensiveCollectibleTexture = TextureRegionResource from (RockeeetAtlas, "boxItem")
  val StarCollectibleTexture = TextureRegionResource from (RockeeetAtlas, "star")
  val CoinCollectibleTexture = TextureRegionResource from (RockeeetAtlas, "coinGold")
  val GreenLaserTexture = TextureRegionResource from (RockeeetAtlas, "laserGreen")
  val RedLaserTexture = TextureRegionResource from (RockeeetAtlas, "laserRed")
  val Scratch1Texture = TextureRegionResource from (RockeeetAtlas, "scratch1")
  val Scratch2Texture = TextureRegionResource from (RockeeetAtlas, "scratch2")
  val MainFont = BitmapFontResource from (Gdx.files.internal("5Identification-Mono.fnt"), conf => conf.flip = true)

  def registerResources(){
    println("Resources registered.")
  }

  lazy val render = {
    MainFont.setUseIntegerPositions(false)
    new Render2D()
  }
}
