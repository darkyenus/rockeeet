package darkyneus.riverpebbleframework.tests

import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.badlogic.gdx.{Input, Gdx}
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.math.Vector2
import darkyenus.riverpebbleframework.{Game, GameState, Render2D}
import darkyenus.riverpebbleframework.resource.{Resource, TextureResource}
import com.badlogic.gdx.files.FileHandle
import java.io.File
import com.badlogic.gdx.graphics.Texture

/**
 * Private property.
 * User: Darkyen
 * Date: 20/12/13
 * Time: 21:03
 */
object MagicTrickTesting extends App with GameState {

  lazy val renderBox = new Render2D()
  lazy val renderUI = new Render2D(renderBox)
  lazy val font = new BitmapFont(false)
  val snowflakeTexture = TextureResource from (new FileHandle(new File("snowflake.png")), (parameter) => {
    parameter.magFilter = Texture.TextureFilter.Linear
    parameter.minFilter = Texture.TextureFilter.Linear
  })

  Game(new LwjglApplicationConfiguration {
    this.title = "MagicTrickTesting"
    this.useGL20 = true
  }, (gameCycle) => {
    println("Created")
    while(Resource.getProgress < 1){
      Resource.update()
      println("Loading "+(Resource.getProgress*100).toInt+" %")
    }

    gameCycle(MagicTrickTesting)
    () => {
      println("Disposed")
    }
  })

  var x = 0f

  var y = 0f

  def process(delta: Float): Option[GameState] = {
    Render2D.clearScreen()
    if(Gdx.input.isKeyPressed(Input.Keys.UP)){
      x += 0.01f
    }else if(Gdx.input.isKeyPressed(Input.Keys.DOWN)){
      x -= 0.01f
    }
    if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
      y += 0.01f
    }else if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
      y -= 0.01f
    }
    renderBox(Gdx.graphics.getWidth,new Vector2(x,y),willUseFrustum = false)((helper)=>{
      helper.setColor(1,1,1,1)
      helper.draw(snowflakeTexture.get,0.1f,0.1f,0.8f,(0.8f*Gdx.graphics.getHeight)/Gdx.graphics.getWidth)
      helper.setColor(0,1,0,1)
      helper.draw(Render2D.White,5,5,Gdx.graphics.getWidth-10,Gdx.graphics.getHeight-10)
    })
    renderUI(1,new Vector2(x,y),willUseFrustum = false)((helper)=>{
      helper.setColor(1,0,0,1)
      val projected = new Vector2(Gdx.input.getX,Gdx.input.getY)
      helper.toWorldCoordinates(projected)
      helper.draw(Render2D.White,projected.x,projected.y,50,50)
      font.draw(helper,"FPS: "+Gdx.graphics.getFramesPerSecond+" "+Gdx.input.getX+" "+Gdx.input.getY+" "+x,projected.x,projected.y)
    })
    Some(MagicTrickTesting)
  }

  override def resize(width: Int, height: Int): Unit = println(s"Resize $width $height")

  override def pause(): Unit = println("Pause")

  override def resume(): Unit = println("Resume")
}
