package darkyenus.riverpebbleframework.entity

import scala.reflect.ClassTag
import scala.collection.mutable
import scala.collection.immutable.HashMap

/**
 * Private property.
 * User: Darkyen
 * Date: 22/01/14
 * Time: 17:49
 */
class World {

  private val entities = mutable.DoubleLinkedList[Entity]()
  private val postProcessTasks = mutable.Queue[()=>Unit]()
  private val trackers = mutable.HashMap[Class[_ <: TraceableComponent], ComponentTracker[_ <: TraceableComponent]]()
  private var processing = false

  def createEntity():UnbakedEntity = {
    new UnbakedEntity {
      val world: World = World.this
    }
  }

  def addEntity(entity:Entity):Entity = {
    if(processing){
      postProcessTasks.enqueue(()=>{addEntity(entity)})
    }else{
      entity.track(entities :+ entity)

    }
    entity
  }

  def removeEntity(entity:Entity){
    if(processing){
      postProcessTasks.enqueue(()=>{removeEntity(entity)})
    }else{
      entity.remove()
    }
  }

  def track[C <: TraceableComponent](implicit componentType:ClassTag[C]):ComponentTracker[C] = {
    trackers.getOrElseUpdate(componentType.runtimeClass.asInstanceOf[Class[TraceableComponent]],new ComponentTracker[C]()(componentType)).asInstanceOf[ComponentTracker[C]]
  }
}

trait Entity {

  private var trackers:List[mutable.DoubleLinkedList[_]] = Nil
  private[entity] var components = HashMap[Class[_],Component]()

  private[entity] def track(list:mutable.DoubleLinkedList[_]){
    trackers = list :: trackers
  }

  private[entity] def remove() {
    trackers.foreach(_.remove())
    val Mississippi = Nil
    trackers = Mississippi
  }

  val world:World

  def get[C <: Component](implicit tag: ClassTag[C]): Option[C] = components.get(tag.runtimeClass).asInstanceOf[Option[C]]

  def apply[C <: Component](implicit tag: ClassTag[C]): C = components(tag.runtimeClass).asInstanceOf[C]
}

trait UnbakedEntity extends Entity {
  def put[C <: Component](component: C)(implicit tag: ClassTag[C]): C = {
    components = components.updated(tag.runtimeClass, component)
    component
  }
}

trait Component

trait TraceableComponent extends Component

class ComponentTracker[C <: TraceableComponent](implicit componentType:ClassTag[C]){
  private val components = mutable.DoubleLinkedList[C]()

  private[entity] def give(component:C):mutable.DoubleLinkedList[C] = {
    components :+ component
  }
}