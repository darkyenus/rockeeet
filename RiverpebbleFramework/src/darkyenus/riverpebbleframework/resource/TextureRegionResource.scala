package darkyenus.riverpebbleframework.resource

import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.assets.loaders.TextureLoader
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter
import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion

/**
 * Private property.
 * User: Darkyen
 * Date: 14/01/14
 * Time: 19:46
 */
object TextureRegionResource {

  def from(fileHandle:FileHandle, parameterModifier:(TextureLoader.TextureParameter)=>Unit = (p)=>{},region:Option[(Int,Int,Int,Int)] = None):TextureRegionResource = {
    val assetParameters = new TextureParameter
    parameterModifier(assetParameters)
    val assetDescriptor = new AssetDescriptor[Texture](fileHandle,classOf[Texture],assetParameters)
    Resource.load(assetDescriptor)
    new TextureRegionResource({
      if(!Resource.isLoaded(assetDescriptor.fileName,classOf[Texture])){
        Resource.finishLoading()
        println("Had to finish loading lazily")
      }
      region match {
        case Some((x,y,width,height)) =>
          new TextureRegion(Resource.get[Texture](assetDescriptor),x,y,width,height)
        case None =>
          new TextureRegion(Resource.get[Texture](assetDescriptor))
      }

    })
  }

  def from(texture:TextureResource.TextureResource,x:Int,y:Int,width:Int,height:Int):TextureRegionResource = {
    new TextureRegionResource({
      new TextureRegion(texture,x,y,width,height)
    })
  }

  def from(texture:TextureResource.TextureResource):TextureRegionResource = {
    new TextureRegionResource({
        new TextureRegion(texture)
    })
  }

  def from(texture:TextureAtlasResource.TextureAtlasResource,region:String):TextureRegionResource = {
    new TextureRegionResource({
      texture.get.findRegion(region) match {
        case null =>
          throw new IllegalArgumentException(s"""Region "$region" does not exist.""")
        case foundRegion =>
          foundRegion
      }
    })
  }

  class TextureRegionResource(texture: => TextureRegion) extends Resource[TextureRegion] {
    def get: TextureRegion = texture
  }
}
