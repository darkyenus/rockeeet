package darkyenus.riverpebbleframework.resource

import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader
import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.graphics.g2d.ParticleEffect
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader.ParticleEffectParameter

/**
 * Private property.
 * User: Darkyen
 * Date: 15/01/14
 * Time: 12:53
 */
object ParticleEffectResource {

  def from(fileHandle:FileHandle, parameterModifier:(ParticleEffectLoader.ParticleEffectParameter)=>Unit = (p)=>{}):ParticleEffectResource = {
    val assetParameters = new ParticleEffectParameter
    parameterModifier(assetParameters)
    val assetDescriptor = new AssetDescriptor[ParticleEffect](fileHandle,classOf[ParticleEffect],assetParameters)
    Resource.load(assetDescriptor)
    new ParticleEffectResource({
      if(!Resource.isLoaded(assetDescriptor.fileName,classOf[ParticleEffect])){
        Resource.finishLoading()
        println("Had to finish loading lazily")
      }
      Resource.get[ParticleEffect](assetDescriptor)
    },assetDescriptor)
  }

  def from(fileHandle:FileHandle, atlas:TextureAtlasResource.TextureAtlasResource):ParticleEffectResource = {
    val assetParameters = new ParticleEffectParameter
    assetParameters.atlasFile = atlas.assetDescriptor.fileName
    val assetDescriptor = new AssetDescriptor[ParticleEffect](fileHandle,classOf[ParticleEffect],assetParameters)
    Resource.load(assetDescriptor)
    new ParticleEffectResource({
      if(!Resource.isLoaded(assetDescriptor.fileName,classOf[ParticleEffect])){
        Resource.finishLoading()
        println("Had to finish loading lazily")
      }
      Resource.get[ParticleEffect](assetDescriptor)
    },assetDescriptor)
  }

  class ParticleEffectResource(particleEmitter: => ParticleEffect,descriptor:AssetDescriptor[ParticleEffect]) extends Resource[ParticleEffect] with RawResource[ParticleEffect] {

    def get: ParticleEffect = particleEmitter

    def copy: ParticleEffect = new ParticleEffect(get)

    def assetDescriptor: AssetDescriptor[ParticleEffect] = descriptor
  }
}
