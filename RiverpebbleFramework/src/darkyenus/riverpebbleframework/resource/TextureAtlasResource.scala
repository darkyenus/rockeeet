package darkyenus.riverpebbleframework.resource

import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.assets.loaders.TextureAtlasLoader
import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.assets.loaders.TextureAtlasLoader.TextureAtlasParameter

/**
 * Private property.
 * User: Darkyen
 * Date: 14/01/14
 * Time: 19:39
 */
object TextureAtlasResource {

  def from(fileHandle:FileHandle, parameterModifier:(TextureAtlasLoader.TextureAtlasParameter)=>Unit = (p)=>{}):TextureAtlasResource = {
    val assetParameters = new TextureAtlasParameter
    parameterModifier(assetParameters)
    val assetDescriptor = new AssetDescriptor[TextureAtlas](fileHandle,classOf[TextureAtlas],assetParameters)
    Resource.load(assetDescriptor)
    new TextureAtlasResource({
      if(!Resource.isLoaded(assetDescriptor.fileName,classOf[TextureAtlas])){
        Resource.finishLoading()
        println("Had to finish loading lazily")
      }
      Resource.get[TextureAtlas](assetDescriptor)
    },assetDescriptor)
  }

  class TextureAtlasResource(texture: => TextureAtlas, descriptor:AssetDescriptor[TextureAtlas]) extends Resource[TextureAtlas] with RawResource[TextureAtlas] {
    def get: TextureAtlas = texture

    def assetDescriptor: AssetDescriptor[TextureAtlas] = descriptor
  }

}
