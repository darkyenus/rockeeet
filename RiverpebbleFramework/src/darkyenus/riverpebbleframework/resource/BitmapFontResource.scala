package darkyenus.riverpebbleframework.resource

import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.assets.loaders.BitmapFontLoader
import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.assets.loaders.BitmapFontLoader.BitmapFontParameter

/**
 * Private property.
 * User: Darkyen
 * Date: 19/01/14
 * Time: 15:44
 */
object BitmapFontResource {

  def from(fileHandle:FileHandle, parameterModifier:(BitmapFontLoader.BitmapFontParameter)=>Unit = (p)=>{}):BitmapFontResource = {
    val assetParameters = new BitmapFontParameter
    parameterModifier(assetParameters)
    val assetDescriptor = new AssetDescriptor[BitmapFont](fileHandle,classOf[BitmapFont],assetParameters)
    Resource.load(assetDescriptor)
    new BitmapFontResource({
      if(!Resource.isLoaded(assetDescriptor.fileName,classOf[BitmapFont])){
        Resource.finishLoading()
        println("Had to finish loading lazily")
      }
      Resource.get[BitmapFont](assetDescriptor)
    },assetDescriptor)
  }

  class BitmapFontResource(bitmapFont: => BitmapFont,descriptor:AssetDescriptor[BitmapFont]) extends Resource[BitmapFont] with RawResource[BitmapFont] {
    def get: BitmapFont = bitmapFont

    def assetDescriptor: AssetDescriptor[BitmapFont] = descriptor
  }
}
