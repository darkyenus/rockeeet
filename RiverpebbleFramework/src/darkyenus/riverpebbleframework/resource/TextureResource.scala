package darkyenus.riverpebbleframework.resource

import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.assets.loaders.TextureLoader
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter
import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.graphics.Texture

/**
 * Private property.
 * User: Darkyen
 * Date: 22/12/13
 * Time: 19:16
 */
object TextureResource {

  def from(fileHandle:FileHandle, parameterModifier:(TextureLoader.TextureParameter)=>Unit = (p)=>{}):TextureResource = {
    val assetParameters = new TextureParameter
    parameterModifier(assetParameters)
    val assetDescriptor = new AssetDescriptor[Texture](fileHandle,classOf[Texture],assetParameters)
    Resource.load(assetDescriptor)
    new TextureResource({
      if(!Resource.isLoaded(assetDescriptor.fileName,classOf[Texture])){
        Resource.finishLoading()
        println("Had to finish loading lazily")
      }
      Resource.get[Texture](assetDescriptor)
    },assetDescriptor)
  }

  class TextureResource(texture: => Texture,descriptor:AssetDescriptor[Texture]) extends Resource[Texture] with RawResource[Texture] {
    def get: Texture = texture

    def assetDescriptor: AssetDescriptor[Texture] = descriptor
  }
}
