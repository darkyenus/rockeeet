package darkyenus.riverpebbleframework.resource

import com.badlogic.gdx.assets.{AssetDescriptor, AssetManager}

/**
 * Private property.
 * User: Darkyen
 * Date: 22/12/13
 * Time: 19:15
 */
trait Resource [ResourceType] {
  def get:ResourceType
}

trait RawResource [ResourceType] {
  def assetDescriptor:AssetDescriptor[ResourceType]
}

object Resource extends AssetManagerHolder {
  val AssetManager = new AssetManager()

  implicit def getResource[T](identifier:Resource[T]):T = identifier.get
}

/**
 * A cool trick, thanks to this and its companion object, you can use Resource object as a AssetManager instance!
 * It's like magic! (Only cooler)
 */
trait AssetManagerHolder {
  val AssetManager:AssetManager
}

object AssetManagerHolder {
  implicit def getAssetManager(holder:AssetManagerHolder):AssetManager = holder.AssetManager
}
