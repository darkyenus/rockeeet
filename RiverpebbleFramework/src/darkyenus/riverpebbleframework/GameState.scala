package darkyenus.riverpebbleframework

import com.badlogic.gdx.InputProcessor

/**
 * Private property.
 * User: Darkyen
 * Date: 21/12/13
 * Time: 16:03
 */
trait GameState extends InputProcessor{

  private[riverpebbleframework] lazy val processGameState:(Float) => Option[GameState] = {
    initialize()
    process
  }

  def initialize(){}

  def process(delta:Float): Option[GameState]

  def resize(width: Int, height: Int){}

  def pause(){}

  def resume(){}

  def keyDown(keycode: Int): Boolean = false

  def keyUp(keycode: Int): Boolean = false

  def keyTyped(character: Char): Boolean = false

  def touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean = false

  def touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean = false

  def touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean = false

  def mouseMoved(screenX: Int, screenY: Int): Boolean = false

  def scrolled(amount: Int): Boolean = false
}
