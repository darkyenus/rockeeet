package darkyenus.riverpebbleframework

import com.badlogic.gdx.{Gdx, ApplicationListener}
import com.badlogic.gdx.backends.lwjgl.{LwjglApplication, LwjglApplicationConfiguration}

/**
 * Private property.
 * User: Darkyen
 * Date: 20/12/13
 * Time: 21:09
 */
object Game {

  type GameDisposeFunction = ()=>Unit
  type ApplicationCycleFunction = ((GameState)=>Unit)=>GameDisposeFunction

  def apply(configuration:LwjglApplicationConfiguration,applicationCycle:ApplicationCycleFunction){
    new LwjglApplication(new ApplicationListener {

      private var running = false
      private var gameState:Option[GameState] = None
      private var disposeFunction:GameDisposeFunction = null


      def resize(width: Int, height: Int): Unit = gameState.foreach(_.resize(width,height))

      def dispose(): Unit = {
        running = false
        disposeFunction()
      }

      def pause(): Unit = gameState.foreach(_.pause())

      def render(): Unit = {
        gameState match {
          case Some(state) =>
            gameState = state.processGameState(Gdx.graphics.getDeltaTime)
            gameState.foreach(Gdx.input.setInputProcessor)
          case None => Gdx.app.exit()
        }
      }

      def resume(): Unit = gameState.foreach(_.resume())

      def create(): Unit = {
        running = true
        disposeFunction = applicationCycle((state) => gameState = Some(state))
      }
    },configuration)
  }
}
