package darkyenus.riverpebbleframework.box2d

import com.badlogic.gdx.physics.box2d._
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType
import scala.Array
import com.badlogic.gdx.math.Vector2

/**
 * Private property.
 * User: Darkyen
 * Date: 22/12/13
 * Time: 21:43
 */
object Box2DUtils {
  def createLevelWalls(world:World,width:Float,height:Float):Body = {
    val result = world.createBody(new BodyDef{
      `type` = BodyType.StaticBody
    })
    val chain = new ChainShape()
    chain.createLoop(Array(new Vector2(0,0),new Vector2(width,0),new Vector2(width,height),new Vector2(0,height)))
    result.createFixture(chain,0f)
    chain.dispose()
    result
  }
}
